import FlightsService from './src/flights.service';
import { Flight } from './src/flights.service';

const flightsService: FlightsService = new FlightsService();

const getLotFlights = async (): Promise<Array<Flight>> => {
  return flightsService.getAll({ callsign: 'lot' });
};

const runTests = async () => {
  console.log('RUNNING TESTS ...');

  // const lotFlights = await getLotFlights()

  // console.log(`LOT flights count: ${lotFlights.length}`)
  // console.log(`First flight: ${JSON.stringify(lotFlights[0])}`)
  // //const flightsWithDetails = await flightsService.getFlightDetailsAllWithOmitErrors([lotFlights[0]])
  // const flightsWithDetails = await flightsService.getFlightDetailsAllWithRetries(lotFlights)
  // const firstFlight = flightsWithDetails[1]

  // console.log(`Aircraft: ${firstFlight.details.aircraft.model.text}`)
  // console.log(`From: ${firstFlight.details.airport.origin.name}`)
  // console.log(`To: ${firstFlight.details.airport.destination.name}`)
  // console.log(`Real: ${firstFlight.details.airport.real? firstFlight.details.airport.real.name : 'NULL'}`)
  // console.log(`Airline: ${firstFlight.details.airline.name}`)
  // console.log(`Aircraft photos: ${JSON.stringify(firstFlight.details.aircraft.images)}`)

  // const flightWihtNoArrivalTime = await flightsService.getFlightDetails('21cc1405')

  const flightWithNoArrivalTime = await flightsService.getFlightDetails(
    '21df9db4'
  );
  console.log(JSON.stringify(flightWithNoArrivalTime.status));
};

runTests();
