# NodeFlights

Aplikacja do pobierania listy lotów linii lotniczej LOT z serwisu FlightRadar24.
Dane o lotach zapisuje w bazie MySQL.

Konfiguracja w pliku .env (wzór w .env-example).

Uruchomienie poprzez skrypty sh:

- get.sh (uruchamiać co ok 20 min) - pobieranie aktualnych lotów (samoloty w powietrzu)
- getDetails.sh (uruchamiać co ok 1h) - pobieranie szczegółów lotu (już po wylądowaniu). Szczegóły typu dokładne czasy startu i lądowania, ewentualne przekierowania itp.
