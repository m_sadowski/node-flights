import Sequelize from 'sequelize';
import logger from './logger';

const env = process.env;

// db schema (sequelize)
const SequelizeExt: any = Sequelize; // Fix error that cannot instantiate new Sequelize object, TODO remove it ...
const sequelize: any = new SequelizeExt(env.DB_NAME, env.DB_USER, env.DB_PASS, {
  dialect: 'mysql',
  host: env.DB_URL,
  logging: false,
  operatorsAliases: false,
  pool: {
    acquire: 30000,
    idle: 20000,
    max: 5,
    min: 0,
  },
  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
});

sequelize
  .authenticate()
  .then(() => {
    // logger.info('[DB] Connection to MySQL established successfully.')
  })
  .catch((err: Error) => {
    logger.error(`[DB] Unable to connect to database. ${err}`);
  });

const FlightDB = sequelize.define(
  'flight',
  {
    aircraft: { type: Sequelize.STRING(10), allowNull: false },
    callsign: { type: Sequelize.STRING(10), allowNull: false },
    id: { type: Sequelize.STRING(15), primaryKey: true },
    // tslint:disable-next-line: object-literal-sort-keys
    from: { type: Sequelize.STRING(10), allowNull: false },
    to: { type: Sequelize.STRING(10), allowNull: false },
    reg: { type: Sequelize.STRING(10), allowNull: true },
    timeSchedDep: { type: Sequelize.DATE, allowNull: true },
    timeSchedArr: { type: Sequelize.DATE, allowNull: true },
    timeRealDep: { type: Sequelize.DATE, allowNull: true },
    timeRealArr: { type: Sequelize.DATE, allowNull: true },
    status: { type: Sequelize.INTEGER, allowNull: true },
    diverted: { type: Sequelize.STRING(10), allowNull: true },
    counter: { type: Sequelize.INTEGER, allowNull: false },
  },
  {
    timestamps: true,
  }
);

const AircraftDB = sequelize.define(
  'aircraft',
  {
    reg: { type: Sequelize.STRING(10), primaryKey: true },
    // tslint:disable-next-line: object-literal-sort-keys
    model: { type: Sequelize.STRING(10), allowNull: false },
    full: { type: Sequelize.STRING(100), allowNull: false },
    images: { type: Sequelize.STRING(4096), allowNull: true }, // json z images
  },
  {
    timestamps: true,
  }
);

const AirlineDB = sequelize.define(
  'airline',
  {
    iata: { type: Sequelize.STRING(10), primaryKey: true },
    icao: { type: Sequelize.STRING(10), allowNull: false },
    name: { type: Sequelize.STRING(100), allowNull: false },
  },
  {
    timestamps: true,
  }
);

const AirportDB = sequelize.define(
  'airport',
  {
    iata: { type: Sequelize.STRING(10), primaryKey: true },
    icao: { type: Sequelize.STRING(10), allowNull: false },
    // tslint:disable-next-line: object-literal-sort-keys
    country: { type: Sequelize.STRING(50), allowNull: false },
    city: { type: Sequelize.STRING(50), allowNull: true },
    latitude: { type: Sequelize.FLOAT, allowNull: false },
    longitude: { type: Sequelize.FLOAT, allowNull: false },
    altitude: { type: Sequelize.FLOAT, allowNull: true },
    timezone: { type: Sequelize.STRING(50), allowNull: false },
    timezoneOffset: { type: Sequelize.INTEGER, allowNull: false },
    website: { type: Sequelize.STRING(256), allowNull: true },
  },
  {
    timestamps: true,
  }
);

sequelize
  .sync()
  .then(() => {
    // logger.info('[DB] Sequelize synced.')
  })
  .catch((err: Error) => {
    logger.error(`[DB] Sequelize sync error. ${err}`);
  });

export { FlightDB, AircraftDB, AirlineDB, AirportDB };
