import dotenv from 'dotenv';
import * as fs from 'fs';
import path from 'path';
dotenv.config({ path: '.env' });

import { literal, Op } from 'sequelize';

import { AircraftDB, AirlineDB, AirportDB, FlightDB } from './db';
import FlightsService, {
  AirportData,
  Flight,
  FlightsQueryParams,
} from './flights/src/flightsService';
import logger from './logger';

/**
 * Returns 1 on create, 2 on update flight record
 * @param flight
 */
const saveFlight = (flight: Flight): Promise<number> => {
  return new Promise((resolve, reject) => {
    const where: any = { id: flight.id };
    const defaultsCreate = { ...flight, counter: 1 };
    const defaultsUpdate: any = { ...flight, counter: literal('counter + 1') };
    FlightDB.findOrCreate({
      defaults: defaultsCreate,
      where,
    })
      .spread(async (fl: any, created: boolean) => {
        if (!created) {
          try {
            await fl.update(defaultsUpdate);
            resolve(2);
          } catch (err) {
            reject(err);
          }
        } else {
          resolve(1);
        }
      })
      .catch((err: Error) => {
        reject(err);
      });
  });
};

/**
 * Save all flights to DB
 * @param flights
 */

const saveFlights = (flights: Flight[]): Promise<any[]> => {
  return new Promise(async resolve => {
    const flightsToSave = flights.map(flight => {
      return saveFlight(flight).catch(err => err); // catch errors as result
    });
    const result = await Promise.all(flightsToSave);
    resolve(result);
  });
};

const saveAircraft = (flight: Flight): Promise<number> => {
  return new Promise(async (resolve, reject) => {
    let result = 0; // 0 - nothing, 1-created, 2-updated
    const details = flight.details;

    if (!details) {
      resolve(result);
      return;
    }

    const aircraft = details.aircraft;

    if (!aircraft) {
      resolve(result);
      return;
    }

    const where = { reg: aircraft.registration };
    const defaults = {
      full: aircraft.model.text,
      images: JSON.stringify(aircraft.images),
      model: aircraft.model.code,
    };
    AircraftDB.findOrCreate({
      defaults,
      where,
    })
      .spread(async (ac: any, created: boolean) => {
        if (!created) {
          try {
            await ac.update({
              full: defaults.full,
              images: defaults.images,
              model: defaults.model,
            });
            result = 2;
            resolve(result);
          } catch (err) {
            reject(err);
          }
        } else {
          result = 1;
          resolve(result);
        }
      })
      .catch((err: Error) => {
        reject(err);
      });
  });
};

const saveAircraftAll = (flights: Flight[]): Promise<any[]> => {
  return new Promise(async resolve => {
    const aircraftsToSave = flights.map(flight => {
      return saveAircraft(flight).catch(err => err); // catch errors as result
    });
    const result = await Promise.all(aircraftsToSave);
    resolve(result);
  });
};

const saveAirline = (flight: Flight): Promise<number> => {
  return new Promise(async (resolve, reject) => {
    let result = 0; // 0 - nothing, 1-created, 2-updated
    const details = flight.details;

    if (!details) {
      resolve(result);
      return;
    }

    const airline = details.airline;

    if (!airline) {
      resolve(result);
      return;
    }

    const where = { iata: airline.code.iata };
    const defaults = { icao: airline.code.icao, name: airline.name };
    AirlineDB.findOrCreate({
      defaults,
      where,
    })
      .spread(async (al: any, created: boolean) => {
        if (!created) {
          try {
            await al.update({
              icao: defaults.icao,
              name: defaults.name,
            });
            result = 2;
            resolve(result);
          } catch (err) {
            reject(err);
          }
        } else {
          result = 1;
          resolve(result);
        }
      })
      .catch((err: Error) => {
        reject(err);
      });
  });
};

const saveAirlineAll = (flights: Flight[]): Promise<any[]> => {
  return new Promise(async resolve => {
    const airlinesToSave = flights.map(flight => {
      return saveAirline(flight).catch(err => err); // catch errors as result
    });
    const result = await Promise.all(airlinesToSave);
    resolve(result);
  });
};

const saveAirport = (airport: AirportData): Promise<number> => {
  return new Promise(async (resolve, reject) => {
    let result = 0; // 0 - nothing, 1-created, 2-updated

    if (!airport) {
      resolve(result);
      return;
    }

    // TODO validation
    const code = airport.code;
    const position = airport.position;
    const timezone = airport.timezone;

    const where = { iata: code.iata };
    const defaults = {
      altitude: position.altitude,
      city: position.region.city ? position.region.city : '',
      country: position.country.name,
      icao: code.icao,
      latitude: position.latitude,
      longitude: position.longitude,
      timezone: timezone.name,
      timezoneOffset: timezone.offset,
      website: airport.website,
    };

    AirportDB.findOrCreate({
      defaults,
      where,
    })
      .spread(async (ap: any, created: boolean) => {
        if (!created) {
          try {
            await ap.update(defaults);
            result = 2;
            resolve(result);
          } catch (err) {
            reject(err);
          }
        } else {
          result = 1;
          resolve(result);
        }
      })
      .catch((err: Error) => {
        reject(err);
      });
  });
};

const saveAirportsAll = (flights: Flight[]): Promise<any[]> => {
  return new Promise(async resolve => {
    const airportsToSavePromises: any[] = [];
    flights.forEach(flight => {
      if (flight.details.airport.origin) {
        airportsToSavePromises.push(saveAirport(flight.details.airport.origin));
      }
      if (flight.details.airport.destination) {
        airportsToSavePromises.push(
          saveAirport(flight.details.airport.destination)
        );
      }
      if (flight.details.airport.real) {
        airportsToSavePromises.push(saveAirport(flight.details.airport.real));
      }
    });
    const result = await Promise.all(airportsToSavePromises);
    resolve(result);
  });
};

const saveDetails = (flight: Flight): Promise<number> => {
  return new Promise(async (resolve, reject) => {
    let result = 0; // 0 - nothing, 1-created, 2-updated
    const details = flight.details;

    if (!details || details.status.live === true) {
      resolve(result);
      return;
    }

    const time = details.time;
    if (!time) {
      resolve(result);
      return;
    }
    const timeScheduled = time.scheduled;
    const timeScheduledDeparture =
      timeScheduled && timeScheduled.departure
        ? new Date(timeScheduled.departure * 1000)
        : null;
    const timeScheduledArrival =
      timeScheduled && timeScheduled.arrival
        ? new Date(timeScheduled.arrival * 1000)
        : null;

    const timeReal = time.real;
    const timeRealDeparture =
      timeReal && timeReal.departure
        ? new Date(timeReal.departure * 1000)
        : null;
    let timeRealArrival =
      timeReal && timeReal.arrival ? new Date(timeReal.arrival * 1000) : null;

    if (!timeRealArrival) {
      // czasem API nie zwraca time.real.arrival (null), wtedy brac z time.estimated.arrival
      const timeEstimated = time.estimated;
      timeRealArrival =
        timeEstimated && timeEstimated.arrival
          ? new Date(timeEstimated.arrival * 1000)
          : null;
    }

    if (!timeRealArrival) {
      // Get time real arrival from details.status.generic.eventTime.utc
      try {
        timeRealArrival = details.status.generic.eventTime
          ? new Date(details.status.generic.eventTime.utc * 1000)
          : null;
        // tslint:disable-next-line: no-empty
      } catch {}
    }

    const diverted = details.status.generic.status.diverted
      ? details.status.generic.status.diverted
      : null;

    const statusColor = details.status.generic.status.color;
    const status = FlightsService.getStatusNumber(statusColor);

    const updateData: any = {
      diverted,
      status,
      timeRealArr: timeRealArrival,
      timeRealDep: timeRealDeparture,
      timeSchedArr: timeScheduledArrival,
      timeSchedDep: timeScheduledDeparture,
    };

    try {
      if (!flight.from) {
        updateData.from = '';
        updateData.from = details.airport.origin.code.iata;
      }
      // tslint:disable-next-line: no-empty
    } catch {}

    try {
      if (!flight.to) {
        updateData.to = '';
        updateData.to = details.airport.destination.code.iata;
      }
      // tslint:disable-next-line: no-empty
    } catch {}

    try {
      if (!flight.callsign) {
        updateData.callsign = '';
        updateData.callsign =
          details.identification.number.default ||
          details.identification.callsign;
      }
      // tslint:disable-next-line: no-empty
    } catch {}

    try {
      if (!flight.reg) {
        updateData.reg = '';
        updateData.reg = details.aircraft.registration;
      }
      // tslint:disable-next-line: no-empty
    } catch {}

    try {
      if (!flight.aircraft) {
        updateData.aircraft = '';
        updateData.aircraft = details.aircraft.model.code;
      }
      // tslint:disable-next-line: no-empty
    } catch {}

    try {
      await FlightDB.update(updateData, {
        where: { id: flight.id },
      });
      result = 2;
      resolve(result);
    } catch (err) {
      reject(err);
    }
  });
};

const saveDetailsAll = (flights: Flight[]): Promise<any[]> => {
  return new Promise(async resolve => {
    const detailsToSave = flights.map(flight => {
      return saveDetails(flight).catch(err => err); // catch errors as result
    });
    const result = await Promise.all(detailsToSave);
    resolve(result);
  });
};

const getFlightsWithoutDetails = (): Promise<Flight[]> => {
  return new Promise(async (resolve, reject) => {
    try {
      let lastFlights = await FlightDB.findAll({
        where: {
          status: {
            [Op.eq]: null, // status NULL
          },
          updatedAt: {
            [Op.lt]: new Date(new Date().getTime() - 60 * 60 * 1000), // Not updated in last 1 hour
          },
        },
      });
      lastFlights = lastFlights.map((fl: any) => fl.get({ plain: true }));
      resolve(lastFlights);
    } catch (err) {
      reject(err);
    }
  });
};

const getFlightsWithoutArrivalTime = (): Promise<Flight[]> => {
  return new Promise(async (resolve, reject) => {
    try {
      let lastFlights = await FlightDB.findAll({
        where: {
          timeRealArr: {
            [Op.eq]: null, // No arrival time
          },
          updatedAt: {
            [Op.gt]: new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000), // Not older than 7 days
            [Op.lt]: new Date(new Date().getTime() - 2 * 60 * 60 * 1000), // Not updated at last 2 hours
          },
        },
      });
      lastFlights = lastFlights.map((fl: any) => fl.get({ plain: true }));
      resolve(lastFlights);
    } catch (err) {
      reject(err);
    }
  });
};

const getFlights = async () => {
  const flights: Flight[] = await FlightsService.getAll({ callsign: 'lot' }); // Key is flight id
  const flightsCount = flights.length;

  const saveResult: any[] = await saveFlights(flights);
  const createdCount = saveResult.filter(x => x === 1).length;
  const updatedCount = saveResult.filter(x => x === 2).length;
  const errors = saveResult.filter((x: any) => x instanceof Error);
  logger.info(
    `[GetFlights] Flights saved successfully. Created: ${createdCount}. Updated: ${updatedCount}`
  );
  if (errors.length > 0) {
    logger.error(
      `[GetFlights] Errors: ${errors.length}. First: ${
        (errors[0] as Error).message
      }`
    );
  }
  logger.info('[GetFlights] DONE.');

  process.exit();
};

const getFlightDetails = async () => {
  const flightsWithoutDetails = await getFlightsWithoutDetails();
  if (flightsWithoutDetails.length > 0) {
    const flightDetails = await FlightsService.getFlightDetailsAllWithRetries(
      flightsWithoutDetails
    );

    // logger.info(`Details count ${flightDetails.length}`)

    const saveDetailsResult = await saveDetailsAll(flightDetails);
    const updatedCount = saveDetailsResult.filter((x: any) => x === 2).length;
    const detailErrors = saveDetailsResult.filter(
      (x: any) => x instanceof Error
    );
    logger.info(`[GetDetails] Flight details updated: ${updatedCount}`);
    if (detailErrors.length > 0) {
      logger.error(
        `[GetDetails] Details errors: ${detailErrors.length}. First: ${
          (detailErrors[0] as Error).message
        }`
      );
    }

    // TODO save aircraft, airport & airline
    const saveAircraftResult = await saveAircraftAll(flightDetails);
    const createdAircraftCount = saveAircraftResult.filter(x => x === 1).length;
    const updatedAircraftCount = saveAircraftResult.filter((x: any) => x === 2)
      .length;
    const aircraftErrors = saveAircraftResult.filter(
      (x: any) => x instanceof Error
    );
    logger.info(
      `[GetDetails] Aircrafts created: ${createdAircraftCount}. Updated: ${updatedAircraftCount}`
    );
    if (aircraftErrors.length > 0) {
      logger.error(
        `[GetDetails] Aircrafts errors: ${aircraftErrors.length}. First: ${
          (aircraftErrors[0] as Error).message
        }`
      );
    }

    const saveAirportResult = await saveAirportsAll(flightDetails);
    const createdAirportCount = saveAirportResult.filter(x => x === 1).length;
    const updatedAirportCount = saveAirportResult.filter((x: any) => x === 2)
      .length;
    const airportErrors = saveAirportResult.filter(
      (x: any) => x instanceof Error
    );
    logger.info(
      `[GetDetails] Airports created: ${createdAirportCount}. Updated: ${updatedAirportCount}`
    );
    if (airportErrors.length > 0) {
      logger.error(
        `[GetDetails] Airport errors: ${airportErrors.length}. First: ${
          (airportErrors[0] as Error).message
        }`
      );
    }
  } else {
    logger.info('[GetDetails] No flights to update details :)');
  }
  logger.info('[GetDetails] DONE.');

  process.exit();
};

const getFlightsWithoutArrival = async () => {
  const flightsWithoutArrival = await getFlightsWithoutArrivalTime();
  if (flightsWithoutArrival.length > 0) {
    const flightDetails = await FlightsService.getFlightDetailsAllWithRetries(
      flightsWithoutArrival
    );

    // logger.info(`Details count ${flightDetails.length}`)

    const saveDetailsResult = await saveDetailsAll(flightDetails);
    const updatedCount = saveDetailsResult.filter((x: any) => x === 2).length;
    const detailErrors = saveDetailsResult.filter(
      (x: any) => x instanceof Error
    );
    logger.info(
      `[GetFlightsWithoutArrival] Flight details updated: ${updatedCount}`
    );
    if (detailErrors.length > 0) {
      logger.info(
        `[GetFlightsWithoutArrival] Details errors: ${
          detailErrors.length
        }. First: ${(detailErrors[0] as Error).message}`
      );
    }

    process.exit();
  } else {
    logger.info(
      '[GetFlightsWithoutArrival] No flights without arrival time :)'
    );
  }
  logger.info('[GetFlightsWithoutArrival] DONE.');
};

const notifyFlightsTest = async () => {
  const area = [53.37, 52.84, 22.61, 23.79]; // Bialystok I Akalice
  const flightsDataFromArea = await FlightsService.getFrMapAreaData(
    area,
    new FlightsQueryParams()
  );
  const flights = FlightsService.getFlightsList(flightsDataFromArea);
  console.log(flights.length);
  flights.forEach(flight => {
    const fl = { ...flight, date: new Date() };
    const file = `${process.env.LOGS_DIR}/notify.txt`;
    // console.log(file);
    fs.appendFileSync(file, JSON.stringify(fl));
  });
};

export {
  getFlights,
  getFlightDetails,
  getFlightsWithoutArrival,
  notifyFlightsTest,
};
