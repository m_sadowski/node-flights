import { createLogger, format, transports } from 'winston';
// import appRoot from 'app-root-path';

const { combine, colorize, timestamp, printf } = format;

const myFormat = printf(info => `${info.timestamp} ${info.level}: ${info.message}`);

const logger = createLogger({
  format: combine(
    timestamp(),
    myFormat
  ),
  transports: [
    new transports.File({ filename: `${process.env.LOGS_DIR}/error.log`, level: 'error' }),
    new transports.File({ filename: `${process.env.LOGS_DIR}/app.log` })
  ]
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new transports.Console({
    format: combine(
      colorize(),
      myFormat
    )
  }));
}

// create a stream object with a 'write' function that will be used by `morgan`
// logger.stream = {
//   write(message) {
//     // use the 'info' log level so the output will be picked up
//     // by both transports (file and console)
//     logger.info(message);
//   },
// };

export default logger;
